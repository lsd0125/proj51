<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'cart';

if(!empty($_SESSION['cart'])) {
    $keys = array_keys($_SESSION['cart']);
    $sql = sprintf("SELECT * FROM products WHERE sid IN (%s)", implode(',', $keys));
    $result = $mysqli->query($sql);
    $cart_data = [];
    while($row=$result->fetch_assoc()){
        $row['qty'] = $_SESSION['cart'][$row['sid']];
        $cart_data[$row['sid']] = $row;
    }
}





?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        .i-cancel {
            cursor: pointer;
            color: red;
        }

    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <pre>
        <?php //print_r($_SESSION['cart']) ?>
        <?php //print_r($result->fetch_all(MYSQLI_ASSOC)) ?>
        <?php //print_r($cart_data) ?>
    </pre>

    <?php if(empty($cart_data)): ?>
購物車裡沒有資料
    <?php else: ?>


    <table class="table table-striped table-dark">
        <thead class="thead-dark">
        <tr>
            <th scope="col">取消</th>
            <th scope="col">封面</th>
            <th scope="col">書名</th>
            <th scope="col">價格</th>
            <th scope="col">數量</th>
            <th scope="col">小計</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($_SESSION['cart'] as $sid => $qty): ?>
        <tr data-sid="<?= $sid ?>">
            <td><i class="fas fa-trash-alt i-cancel"></i></td>
            <td><img src="./imgs/small/<?= $cart_data[$sid]['book_id'] ?>.jpg" alt=""></td>
            <td><?= $cart_data[$sid]['bookname'] ?></td>
            <td class="money price" data-val="<?= $cart_data[$sid]['price'] ?>"></td>
            <td>

                <select class="i-qty">
                    <?php for($i=1; $i<=20; $i++): ?>
                        <option value="<?= $i ?>" <?= $i==$qty ? 'selected' : '' ?>><?= $i ?></option>
                    <?php endfor; ?>
                </select>


            </td>
            <td class="money sub-total" data-val="<?= $cart_data[$sid]['price'] * $qty ?>"></td>

        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


        <div class="alert alert-primary" role="alert">總計: <span id="total-price">0</span></div>

        <div class="row">
            <div class="col">
                <?php if(isset($_SESSION['user'])): ?>
                <a class="btn btn-primary" href="buy_confirm.php">確定購買</a>
                <?php else: ?>
                <a class="btn btn-danger">請先登入再結帳</a>
                <?php endif; ?>
            </div>
        </div>

    <?php endif; ?>

    <script>
        var dallorCommas = function(n){
            return '$ ' + n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        };

        $('.money').each(function() {
            var val = $(this).attr('data-val');
            $(this).text( dallorCommas(val));
        });

        // 計算總價
        var countTotal = function(){
            var t = 0;
            $('.sub-total').each(function(){
               var n = $(this).attr('data-val');
               n = parseInt(n);
               t += n;
            });

            $('#total-price').text( dallorCommas(t) );
        };
        countTotal();


        // 移除項目

        $('.i-cancel').click(function(){
            var sid = $(this).closest('tr').attr('data-sid');
            var i_cancel = $(this);
            $.get('add_to_cart.php', {sid:sid}, function(data){
                //location.href = location.href; // page reload
                i_cancel.closest('tr').remove();
                countItems(data);
                countTotal();
            }, 'json');


        });

        // 修改數量
        $('.i-qty').change(function(){
            var tr = $(this).closest('tr');
            var sid = tr.attr('data-sid');
            var i_qty = $(this);
            var qty = i_qty.val();
            $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
                var sub_total = qty * tr.find('.price').attr('data-val');
                var $sub_total = tr.find('.sub-total');
                $sub_total.attr('data-val', sub_total);
                $sub_total.text( dallorCommas(sub_total) );

                countItems(data);
                countTotal();
            }, 'json');

        });


    </script>
</div>
<?php include __DIR__. '/__html_foot.php'; ?>