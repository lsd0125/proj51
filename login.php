<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'login';


$email = '';

if(isset($_POST['email'])){

    $email = strip_tags(trim($_POST['email']));

    // 後端檢查必填欄位

    $sql = sprintf("SELECT `id`, `email`, `mobile`, `address`, `birthday`, `nickname`
 FROM `members` WHERE `email`='%s' AND `password`='%s'",
        $mysqli->escape_string($email),
        sha1($_POST['password'])
        );

    $result = $mysqli->query($sql);

    $logined = false;

    //$result->num_rows
    if($result->num_rows == 1){
        $logined = true;

        $row = $result->fetch_assoc();
        $_SESSION['user'] = $row;
    }

}



?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning{
            display: none;
            color: red !important;
        }
        .red_star {
            color: red;
            font-size: x-large;
        }
        .card-title {
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php
    if(isset($logined)) {
        if ($logined) {
            echo '<div class="alert alert-success" role="alert">完成登入</div>';
        } else {
            echo '<div class="alert alert-danger" role="alert">密碼錯誤!</div>';
        }
    }
    ?>
    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">會員登入</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="email"><i class="red_star">*</i> 電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $email ?>"
                               placeholder="">
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="password"><i class="red_star">*</i> 密碼</label>
                        <input type="password" class="form-control" name="password" id="password"
                               value=""
                               placeholder="">
                        <small id="passwordWarning" class="form-text text-muted warning">請輸入六個字元以上的密碼</small>

                    </div>
                    <?php if(!(isset($logined) and $logined)): ?>
                    <button type="submit" class="btn btn-primary">登入</button>
                    <?php endif; ?>
                </form>

            </div>
        </div>



    </div>



    <script>

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var email = document.form1.email.value;
            var password = document.form1.password.value;
            var isPass = true;

            if( password.length<6 ){
                $('#passwordWarning').show();
                isPass = false;

            }

            if(! pattern.test(email) ){
                $('#emailWarning').show();
                isPass = false;

            }




            return isPass;
        }

        <?php if(isset($logined) and $logined): ?>
        setTimeout(function(){
            location.href = './index_.php';
        }, 3000);
        <?php endif; ?>
    </script>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>