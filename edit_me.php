<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'edit_me';

if(! isset($_SESSION['user'])){
    header('Location: ./');
    exit;
}


//
//$nickname = '';
//
//$email = '';
//$mobile = '';
//$birthday = '';
//$address = '';

if(isset($_POST['password'])){
    // 先檢查密碼是否正確
    $c_sql = sprintf("SELECT 1 FROM `members` WHERE `id`='%s' AND `password`='%s'",
        intval($_SESSION['user']['id']),
        sha1($_POST['password'])
    );
    $c_result = $mysqli->query($c_sql);

//    echo $c_sql;
//    exit;

    $bad_pass = true; // 預設密碼是錯的
    if($c_result->num_rows == 1){
        $bad_pass = false; // 密碼是對的


        $nickname = strip_tags(trim($_POST['nickname']));
        $mobile = strip_tags(trim($_POST['mobile']));
        $birthday = strip_tags(trim($_POST['birthday']));
        $address = strip_tags(trim($_POST['address']));

        $sql = "UPDATE `members` SET `mobile`=?,`address`=?,`birthday`=?,`nickname`=?
                WHERE `id`=?";

        $stmt = $mysqli->prepare($sql);

        $stmt->bind_param('ssssi',
            $mobile,
            $address,
            $birthday,
            $nickname,
            $_SESSION['user']['id']
        );

        $stmt->execute();
        $msg_code = $stmt->affected_rows;
        if($msg_code==1){
            // 更新 session
            $_SESSION['user']['mobile'] = $mobile;
            $_SESSION['user']['address'] = $address;
            $_SESSION['user']['birthday'] = $birthday;
            $_SESSION['user']['nickname'] = $nickname;

        }

    }

}






?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning{
            display: none;
            color: red !important;
        }
        .red_star {
            color: red;
            font-size: x-large;
        }
        .card-title {
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php
    //echo (isset($bad_pass) and $bad_pass) ? "\$bad_pass=true" : '';

    if(isset($bad_pass)){
        if($bad_pass){
            echo '<div class="alert alert-danger" role="alert">密碼錯誤!</div>';
        } else {
            if($msg_code==1){
                echo '<div class="alert alert-success" role="alert">修改完成</div>';
            } else {
                echo '<div class="alert alert-info" role="alert">資料沒有變更</div>';
            }

        }

    }

    ?>


    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">編輯個人資料</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $_SESSION['user']['email'] ?>"
                               placeholder="" disabled>
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="password"><i class="red_star">*</i> 原密碼 (密碼不變更)</label>
                        <input type="password" class="form-control" name="password" id="password"
                               value=""
                               placeholder="">
                        <small id="passwordWarning" class="form-text text-muted warning">請輸入六個字元以上的密碼</small>

                    </div>
                    <div class="form-group">
                        <label for="nickname"><i class="red_star">*</i> 暱稱</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="<?= $_SESSION['user']['nickname'] ?>"
                               placeholder="請填入暱稱">
                        <small id="nicknameWarning" class="form-text text-muted warning">請填寫兩個字以上的暱稱</small>
                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" name="mobile" id="mobile"
                               value="<?= $_SESSION['user']['mobile'] ?>"
                               placeholder="">
                        <small id="mobileWarning" class="form-text text-muted warning">請輸入十位數的手機號碼</small>

                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                               value="<?= $_SESSION['user']['birthday'] ?>"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?= $_SESSION['user']['address'] ?>"
                               placeholder="">
                    </div>



                <?php if(!isset($msg_code) or  $msg_code != 1): ?>
                    <button type="submit" class="btn btn-primary">修改</button>
                <?php endif; ?>
                </form>

            </div>
        </div>



    </div>








    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var nickname = document.form1.nickname.value;
            var password = document.form1.password.value;
            var isPass = true;


            if( password.length<6 ){
                $('#passwordWarning').show();
                isPass = false;

            }
            if( nickname.length<2 ){
                $('#nicknameWarning').show();
                isPass = false;

            }

            return isPass;
        }

        /*

        /[A-Z]{2}\d{8}/
        /09\d{8}/
        /^09\d{8}$/

         */

    </script>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>