<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'register';


$nickname = '';

$email = '';
$mobile = '';
$birthday = '';
$address = '';

if(isset($_POST['email'])){
    $nickname = strip_tags(trim($_POST['nickname']));
    $email = strip_tags(trim($_POST['email']));
    $mobile = strip_tags(trim($_POST['mobile']));
    $birthday = strip_tags(trim($_POST['birthday']));
    $address = strip_tags(trim($_POST['address']));

    // 後端檢查必填欄位

    $sql = "INSERT INTO `members`(
        `email`, `password`, `mobile`,
        `address`, `birthday`, `hash`, 
        `nickname`, `create_at`) 
        VALUES (
        ?, ?, ?,
        ?, ?, ?,
        ?, NOW()
        )";

    $stmt = $mysqli->prepare($sql);

    $password = sha1($_POST['password']);
    $hash = sha1($email. $nickname. rand());


    $stmt->bind_param('sssssss',
        $email,
        $password,
        $mobile,

        $address,
        $birthday,
        $hash,

        $nickname
    );

    $stmt->execute();


    $msg_code = $stmt->affected_rows;

}



?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning{
            display: none;
            color: red !important;
        }
        .red_star {
            color: red;
            font-size: x-large;
        }
        .card-title {
            font-weight: bold;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php
    if(isset($msg_code)) {
        switch ($msg_code) {
            case 1:
                echo '<div class="alert alert-success" role="alert">
  會員註冊完成
</div>';

                break;

            case -1:
                echo '<div class="alert alert-danger" role="alert">
  會員註冊錯誤! 此 Email 已被註冊過!
</div>';

                break;
            default:
                echo '<div class="alert alert-danger" role="alert">
  會員註冊錯誤!
</div>';

        }
    }
    ?>


    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">新增資料</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="email"><i class="red_star">*</i> 電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $email ?>"
                               placeholder="">
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="password"><i class="red_star">*</i> 密碼</label>
                        <input type="password" class="form-control" name="password" id="password"
                               value=""
                               placeholder="">
                        <small id="passwordWarning" class="form-text text-muted warning">請輸入六個字元以上的密碼</small>

                    </div>
                    <div class="form-group">
                        <label for="nickname"><i class="red_star">*</i> 暱稱</label>
                        <input type="text" class="form-control" name="nickname" id="nickname"
                               value="<?= $nickname ?>"
                               placeholder="請填入暱稱">
                        <small id="nicknameWarning" class="form-text text-muted warning">請填寫兩個字以上的暱稱</small>
                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" name="mobile" id="mobile"
                               value="<?= $mobile ?>"
                               placeholder="">
                        <small id="mobileWarning" class="form-text text-muted warning">請輸入十位數的手機號碼</small>

                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                               value="<?= $birthday ?>"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?= $address ?>"
                               placeholder="">
                    </div>



                <?php if(!isset($msg_code) or  $msg_code != 1): ?>
                    <button type="submit" class="btn btn-primary">新增</button>
                <?php endif; ?>
                </form>

            </div>
        </div>



    </div>








    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var nickname = document.form1.nickname.value;
            var email = document.form1.email.value;
            var mobile = document.form1.mobile.value;
            var password = document.form1.password.value;
            var isPass = true;

            // mobile = mobile.trim();
            // mobile = mobile.split('-').join('');
            if( password.length<6 ){
                $('#passwordWarning').show();
                isPass = false;

            }
            if( nickname.length<2 ){
                $('#nicknameWarning').show();
                isPass = false;

            }
            if(! pattern.test(email) ){
                $('#emailWarning').show();
                isPass = false;

            }
            // if(! /^09\d{8}$/.test(mobile) ){
            //     $('#mobileWarning').show();
            //     isPass = false;
            //
            // }



            return isPass;
        }

        /*

        /[A-Z]{2}\d{8}/
        /09\d{8}/
        /^09\d{8}$/

         */

    </script>

</div>
<?php include __DIR__. '/__html_foot.php'; ?>