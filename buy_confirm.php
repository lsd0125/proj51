<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'buy_confirm';

if(! isset($_SESSION['user'])){
    header('Location: product_list.php');
    exit;
}

if(empty($_SESSION['cart'])){
    header('Location: product_list.php');
    exit;
}

$user_id = $_SESSION['user']['id']; //會員編號
$total_amount = 0; //總價

$keys = array_keys($_SESSION['cart']);
$sql = sprintf("SELECT * FROM products WHERE sid IN (%s)", implode(',', $keys));
$result = $mysqli->query($sql);
$cart_data = [];
while($row=$result->fetch_assoc()){
    $row['qty'] = $_SESSION['cart'][$row['sid']];
    $cart_data[$row['sid']] = $row;

    $total_amount += $row['qty'] * $row['price'];
}

$sql = "INSERT INTO `orders`(`member_sid`, `amount`, `order_date`) VALUES (?, ?, NOW())";

$stmt = $mysqli->prepare($sql);
$stmt->bind_param('ii',$user_id, $total_amount );
$stmt->execute();

$order_sid = $stmt->insert_id; // order primary key
$stmt->close();


// INSERT INTO `order_details`(`sid`, `order_sid`, `product_sid`, `price`, `quantity`) VALUES (
$d_sql = "INSERT INTO `order_details`(`order_sid`, `product_sid`, `price`, `quantity`) VALUES (?, ?, ?, ?)";
$d_stmt = $mysqli->prepare($d_sql);

foreach($keys as $p_sid){

    $d_stmt->bind_param("iiii",
        $order_sid,
        $p_sid,
        $cart_data[$p_sid]['price'],
        $cart_data[$p_sid]['qty']
        );

    $d_stmt->execute();


}

$d_stmt->close();

unset($_SESSION['cart']); //清除購物車的內容

?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>



    <div class="alert alert-success" role="alert">
        感謝您的購買
        <!--
        <?= $user_id. " : ". $total_amount. " : ". $order_sid ?>
        -->
    </div>



</div>
<?php include __DIR__. '/__html_foot.php'; ?>